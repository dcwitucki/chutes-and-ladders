package edu.bsu.cs.cs222.sal.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import edu.bsu.cs.cs222.sal.core.ChutesAndLadders;

public class ChutesAndLaddersJava {

  public static void main(String[] args) {
    JavaPlatform.Config config = new JavaPlatform.Config();
    // use config to customize the Java platform, if needed
    config.height = 600;
    config.width = 650;
    JavaPlatform.register(config);
    PlayN.run(new ChutesAndLadders());
  }
}
