package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import playn.core.Image;
import playn.core.ImageLayer;

public class PlayerSelectSprite {

	private ImageLayer layer;

	public PlayerSelectSprite(String imagePath) {
		Image selectImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(selectImage);
	}

	public ImageLayer getLayer() {
		return layer;
	}
}
