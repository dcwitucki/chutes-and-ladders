package edu.bsu.cs.cs222.sal.domain;

import java.util.ArrayList;
import com.google.common.collect.Lists;
import static playn.core.PlayN.log;

public class Board {

	private ArrayList<Tile> tiles;
	private final int NUMBER_OF_TILES = 100;

	public Board() {
		initBoard();
	}

	public ArrayList<Tile> getTiles() {
		return tiles;
	}

	private void initBoard() {
		tiles = Lists.newArrayList();
		for (int i = 0; i < NUMBER_OF_TILES; i++) {
			tiles.add(Tile.getBlankTileWithIndex(i + 1));
		}

		// one based index values
		setLadderTile(1, 38);
		setLadderTile(4, 14);
		setLadderTile(9, 31);
		setChuteTile(16, 6);
		setLadderTile(21, 42);
		setLadderTile(28, 84);
		setLadderTile(36, 44);
		setChuteTile(47, 26);
		setChuteTile(49, 11);
		setLadderTile(51, 67);
		setChuteTile(56, 53);
		setChuteTile(62, 19);
		setChuteTile(64, 60);
		setLadderTile(71, 91);
		setLadderTile(80, 100);
		setChuteTile(87, 24);
		setChuteTile(93, 73);
		setChuteTile(95, 75);
		setChuteTile(98, 78);

	}

	private void setLadderTile(int homeTileIndex, int connectingTileIndex) {
		if (homeTileIndex > connectingTileIndex) {
			log().debug(
					"this will not work dummy. Home Tile Index is "
							+ homeTileIndex);
			return;
		}
		try {
			tiles.get(homeTileIndex - 1).setTileTypeLadder(
					tiles.get(connectingTileIndex - 1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setChuteTile(int homeTileIndex, int connectingTileIndex) {
		if (homeTileIndex < connectingTileIndex) {
			log().debug(
					"this will not work dummy. Home Tile Index is "
							+ homeTileIndex);
			return;
		}
		try {
			tiles.get(homeTileIndex - 1).setTileTypeChute(
					tiles.get(connectingTileIndex - 1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
