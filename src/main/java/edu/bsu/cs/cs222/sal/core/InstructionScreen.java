package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import pythagoras.f.Point;
import react.SignalView;
import react.UnitSignal;
import react.UnitSlot;
import tripleplay.game.UIAnimScreen;
import tripleplay.ui.Button;
import tripleplay.ui.Root;
import tripleplay.ui.SimpleStyles;
import tripleplay.ui.layout.AbsoluteLayout;

public class InstructionScreen extends UIAnimScreen {

	private UnitSignal onTitleScreen = new UnitSignal();
	private Root root;
	private InstructionSprite instructionSprite;

	public SignalView<Void> onInstructionScreen() {
		return onTitleScreen;
	}

	@Override
	public void wasAdded() {
		initRoot();
		addBackButton();
		initBoard();

	}

	private void initRoot() {
		root = iface.createRoot(new AbsoluteLayout(), //
				SimpleStyles.newSheet(), //
				layer)//
				.setSize(graphics().width(), graphics().height());
	}

	private void initBoard() {
		log().debug("Initialized Board");
		instructionSprite = new InstructionSprite(
				"images/INSTRUCTION_SCREEN.gif");
		layer.add(instructionSprite.getLayer());
	}
	

	private void addBackButton() {
		Button backButton = new Button("exit this game now\n");
		backButton.setVisible(true);
		backButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				log().debug("Going to TitleScreen");
				onTitleScreen.emit();
			}
		});
		root.add(AbsoluteLayout.at(backButton, new Point(10, 10)));

	}

	@Override
	public void wasShown() {
		super.wasShown();
		log().debug("InstructionScreen shown");
	}

	@Override
	public void wasHidden() {
		super.wasHidden();
		log().debug("InstructionScreen hidden");
	}

	@Override
	public void wasRemoved() {
		super.wasRemoved();
		log().debug("InstructionScreen removed");
	}
}
