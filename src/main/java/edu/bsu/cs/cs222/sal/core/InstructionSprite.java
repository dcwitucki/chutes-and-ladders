package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import playn.core.Image;
import playn.core.ImageLayer;

public class InstructionSprite {

	private ImageLayer layer;

	public InstructionSprite(String imagePath) {
		Image winnerImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(winnerImage);
	}

	public ImageLayer getLayer() {
		return layer;
	}
}
