package edu.bsu.cs.cs222.sal.domain;

import java.util.Random;

public class Spinner {

	public int spinValue;

	public int getSpin() {	
		Random number = new Random();
		spinValue = number.nextInt(6) + 1;
		return spinValue;
	}

}
