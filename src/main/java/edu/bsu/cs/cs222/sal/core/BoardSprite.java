package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import playn.core.Image;
import playn.core.ImageLayer;
import pythagoras.f.Point;
import edu.bsu.cs.cs222.sal.domain.Board;

public class BoardSprite {
	private Board board;
	private ImageLayer layer;
	public float X_OFFSET = 8f;
	public float Y_OFFSET = 8f;

	public BoardSprite(Board aBoard, String imagePath) {
		this.board = aBoard;
		Image boardImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(boardImage);
	}

	public ImageLayer getLayer() {
		return layer;
	}

	public Board getBoard() {
		return board;
	}

	public Point getPointForPlayer(int playerTurn, int tilePosition) {
		log().debug("Player is at tile position " + tilePosition);
		float x = generateX(tilePosition);
		float y = generateY(tilePosition);
		log().debug("Player sprite displayed at " + x + "," + y + ")");
		return new Point(x, y);
	}

	private float generateY(int tilePosition) {
		double tileRange = (double) tilePosition;
		if (Math.ceil(tileRange / 10) == 1)
			return 460 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 2)
			return 410 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 3)
			return 360 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 4)
			return 310 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 5)
			return 260 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 6)
			return 210 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 7)
			return 160 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 8)
			return 110 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 9)
			return 60 + Y_OFFSET;
		else if (Math.ceil(tileRange / 10) == 10)
			return 10 + Y_OFFSET;
		else
			return 510 + Y_OFFSET;

	}

	private float generateX(int tilePosition) {

		if (tilePosition == 1 || tilePosition == 20 || tilePosition == 21
				|| tilePosition == 40 || tilePosition == 41
				|| tilePosition == 60 || tilePosition == 61
				|| tilePosition == 80 || tilePosition == 81
				|| tilePosition == 100)
			return 10 + X_OFFSET;
		else if (tilePosition == 2 || tilePosition == 19 || tilePosition == 22
				|| tilePosition == 39 || tilePosition == 42
				|| tilePosition == 59 || tilePosition == 62
				|| tilePosition == 79 || tilePosition == 82
				|| tilePosition == 99)
			return 60 + X_OFFSET;
		else if (tilePosition == 3 || tilePosition == 18 || tilePosition == 23
				|| tilePosition == 38 || tilePosition == 43
				|| tilePosition == 58 || tilePosition == 63
				|| tilePosition == 78 || tilePosition == 83
				|| tilePosition == 98)
			return 110 + X_OFFSET;
		else if (tilePosition == 4 || tilePosition == 17 || tilePosition == 24
				|| tilePosition == 37 || tilePosition == 44
				|| tilePosition == 57 || tilePosition == 64
				|| tilePosition == 77 || tilePosition == 84
				|| tilePosition == 97)
			return 160 + X_OFFSET;
		else if (tilePosition == 5 || tilePosition == 16 || tilePosition == 25
				|| tilePosition == 36 || tilePosition == 45
				|| tilePosition == 56 || tilePosition == 65
				|| tilePosition == 76 || tilePosition == 85
				|| tilePosition == 96)
			return 210 + X_OFFSET;
		else if (tilePosition == 6 || tilePosition == 15 || tilePosition == 26
				|| tilePosition == 35 || tilePosition == 46
				|| tilePosition == 55 || tilePosition == 66
				|| tilePosition == 75 || tilePosition == 86
				|| tilePosition == 95)
			return 260 + X_OFFSET;
		else if (tilePosition == 7 || tilePosition == 14 || tilePosition == 27
				|| tilePosition == 34 || tilePosition == 47
				|| tilePosition == 54 || tilePosition == 67
				|| tilePosition == 74 || tilePosition == 87
				|| tilePosition == 94)
			return 310 + X_OFFSET;
		else if (tilePosition == 8 || tilePosition == 13 || tilePosition == 28
				|| tilePosition == 33 || tilePosition == 48
				|| tilePosition == 53 || tilePosition == 68
				|| tilePosition == 73 || tilePosition == 88
				|| tilePosition == 93)
			return 360 + X_OFFSET;
		else if (tilePosition == 9 || tilePosition == 12 || tilePosition == 29
				|| tilePosition == 32 || tilePosition == 49
				|| tilePosition == 52 || tilePosition == 69
				|| tilePosition == 72 || tilePosition == 89
				|| tilePosition == 92)
			return 410 + X_OFFSET;
		else if (tilePosition == 10 || tilePosition == 11 || tilePosition == 30
				|| tilePosition == 31 || tilePosition == 50
				|| tilePosition == 51 || tilePosition == 70
				|| tilePosition == 71 || tilePosition == 90
				|| tilePosition == 91)
			return 460 + X_OFFSET;
		else
			return 10 + X_OFFSET;

	}
}
