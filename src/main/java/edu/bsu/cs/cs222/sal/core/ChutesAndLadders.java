package edu.bsu.cs.cs222.sal.core;

import playn.core.Game;
import playn.core.util.Clock;
import react.UnitSlot;
import tripleplay.game.ScreenStack;
import edu.bsu.cs.cs222.sal.domain.ChutesAndLaddersGame;

public class ChutesAndLadders extends Game.Default {

	private static final int UPDATE_RATE = 33;
	private ScreenStack screenStack;
	private Clock.Source clock = new Clock.Source(UPDATE_RATE);
	private PlayerSelectScreen selectScreen;
	private GameScreen gameScreen;
	private InstructionScreen instructionScreen;
	private TitleScreen titleScreen;
	private WinnerScreen winnerScreen;
	private PlayerSelectScreen playerSelectScreen;
	private ChutesAndLaddersGame game;
	boolean onePlayer = false;
	boolean twoPlayer = false;
	boolean threePlayer = false;
	boolean fourPlayer = false;

	public ChutesAndLadders() {
		super(UPDATE_RATE); // call update every 33ms (30 times per second)
	}

	@Override
	public void init() {
		initScreens();
		//SwitchToGameScreen();
		SwitchToTitleScreen();
		SwitchToInstructionScreen();
		SwitchToWinnerScreen();
		SwitchToTitleFromGame();
		SwitchToGameFromWinScreen();
		SwitchToTitleFromWinScreen();
		SwitchToSelectScreenFromTitle();
		PlayGameWithOnePlayer();
		PlayGameWithTwoPlayers();
		PlayGameWithThreePlayers();
		PlayGameWithFourPlayers();
		initScreenStack();
	}

	private void initScreenStack() {
		screenStack = new ScreenStack();
		screenStack.push(titleScreen);
	}

	@Override
	public void update(int deltaMS) {
		clock.update(deltaMS);
		screenStack.update(deltaMS);

	}

	@Override
	public void paint(float alpha) {
		clock.paint(alpha);
		screenStack.paint(clock);

	}

	/* void SwitchToGameScreen() {
		titleScreen.onStartGame().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}
*/
	private void SwitchToTitleFromGame() {
		gameScreen.backToTitle().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(titleScreen);
			}
		});
	}

	public void PlayGameWithOnePlayer() {
		playerSelectScreen.on1Player().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}

	private void PlayGameWithTwoPlayers() {
		playerSelectScreen.on2Player().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}

	private void PlayGameWithThreePlayers() {
		playerSelectScreen.on3Player().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}

	private void PlayGameWithFourPlayers() {
		playerSelectScreen.on4Player().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}

	private void SwitchToInstructionScreen() {
		titleScreen.onInstructions().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(instructionScreen);

			}
		});
	}

	private void SwitchToTitleFromWinScreen() {
		winnerScreen.onMainMenu().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(titleScreen);	

			}
		});
	}

	private void SwitchToSelectScreenFromTitle() {
		titleScreen.onStartGame().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(playerSelectScreen);
			}
		});
	}

	private void SwitchToWinnerScreen() {
		gameScreen.onGameScreen().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(winnerScreen);
			}

		});
	}

	private void SwitchToGameFromWinScreen() {
		winnerScreen.onRestart().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(gameScreen);
			}
		});
	}

	private void SwitchToTitleScreen() {
		instructionScreen.onInstructionScreen().connect(new UnitSlot() {
			@Override
			public void onEmit() {
				screenStack.replace(titleScreen);

			}
		});
	}


	private void initScreens() {
		titleScreen = new TitleScreen();
		instructionScreen = new InstructionScreen();
		playerSelectScreen = new PlayerSelectScreen();
		gameScreen = new GameScreen();
		winnerScreen = new WinnerScreen();
		
	}

	public PlayerSelectScreen getSelectScreen() {
		return selectScreen;
	}

	public void setSelectScreen(PlayerSelectScreen selectScreen) {
		this.selectScreen = selectScreen;
	}

	public ChutesAndLaddersGame getGame() {
		return game;
	}

	public void setGame(ChutesAndLaddersGame game) {
		this.game = game;
	}

}
