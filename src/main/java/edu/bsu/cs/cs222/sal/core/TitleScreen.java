package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import pythagoras.f.Point;
import react.SignalView;
import react.UnitSignal;
import react.UnitSlot;
import tripleplay.game.UIAnimScreen;
import tripleplay.ui.Button;
import tripleplay.ui.Root;
import tripleplay.ui.SimpleStyles;
import tripleplay.ui.layout.AbsoluteLayout;

//import tripleplay.ui.Label;

public class TitleScreen extends UIAnimScreen {

	private Root root;
	TitleSprite background;
	TitleSprite playButton = new TitleSprite("images/START_BUTTON.png");
	TitleSprite instructionButton = new TitleSprite(
			"images/INSTRUCTIONS_BUTTON.png");
	TitleSprite exitButton = new TitleSprite("images/EXIT_BUTTON.png");

	private UnitSignal onStartGame = new UnitSignal();
	private UnitSignal onInstructions = new UnitSignal();

	public SignalView<Void> onStartGame() {
		return onStartGame;
	}

	public SignalView<Void> onInstructions() {
		return onInstructions;
	}

	@Override
	public void wasAdded() {
		
		initRoot();
		titleBackground();
		addButtons();
		addStartButton();
		addInstructionButton();
		addExitButton();
	
		log().debug("Title Screen Created");

	}

	private void titleBackground() {
		log().debug("Added Background Image");
		background = new TitleSprite("images/TITLE_SCREEN.gif");
		layer.add(background.getLayer());
	}

	private void initRoot() {
		root = iface.createRoot(new AbsoluteLayout(), //
				SimpleStyles.newSheet(), //
				layer)//
				.setSize(graphics().width(), graphics().height());
	}

	private void addButtons() {
		log().debug("added Buttons to layer ( playButton, instructionButton, exitButton )");
		layer.add(playButton.getLayer().setOrigin(-249, -250));
		layer.add(instructionButton.getLayer().setOrigin(-249, -360));
		layer.add(exitButton.getLayer().setOrigin(-249, -465));
	}

	private void addStartButton() {
		Button playButton = new Button("play this game now\n");
		playButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				onStartGame.emit();
			}
		});
		root.add(AbsoluteLayout.at(playButton, new Point(250, 250)));

	}

	private void addInstructionButton() {
		Button instructionButton = new Button("Instructionpagenow\n");
		instructionButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				onInstructions.emit();
			}
		});
		root.add(AbsoluteLayout.at(instructionButton, new Point(250, 360)));

	}

	private void addExitButton() {
		Button instructionButton = new Button("exit this game now\n");
		instructionButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				System.exit(0);
			}
		});
		root.add(AbsoluteLayout.at(instructionButton, new Point(255, 465)));

	}

	@Override
	public void wasShown() {
		super.wasShown();
		log().debug("TitleScreen shown");
	}

	@Override
	public void wasHidden() {
		super.wasHidden();
		log().debug("TitleScreen hidden");
	}

	@Override
	public void wasRemoved() {
		super.wasRemoved();
		log().debug("TitleScreen removed");
	}

}
