package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import edu.bsu.cs.cs222.sal.domain.Spinner;
import playn.core.Image;
import playn.core.ImageLayer;

public class SpinnerSprite {
	private ImageLayer spinLayer;
	private Spinner spinner;
	
	public SpinnerSprite(String imagePath){
		Image playerImage = assets().getImage(imagePath);
		spinLayer = graphics().createImageLayer(playerImage);
		spinLayer.setDepth(1);
	}
	
	public ImageLayer getLayer(){
		return spinLayer;
	}
	
	public Spinner getSpinner(){
		return spinner;
	}
	
}
