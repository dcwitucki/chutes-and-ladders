package edu.bsu.cs.cs222.sal.domain;

import static playn.core.PlayN.log;

import java.util.ArrayList;
import java.util.Random;

import com.google.common.collect.Lists;

public class ChutesAndLaddersGame {

	private Board board;
	private ArrayList<Player> players;
	private Spinner spinner;
	private int order;
	private final int MAX_NUMBER_OF_PLAYERS = 4;
	public boolean foundPlayer = false;


	public ChutesAndLaddersGame() {
		board = new Board();
		players = Lists.newArrayList();
		spinner = new Spinner();

	}

	public void setNumberOfPlayers(int numberOfPlayers) throws Exception {
		if (numberOfPlayers > MAX_NUMBER_OF_PLAYERS)
			log().debug(
					"Whooaaa! that's too many players. Must be 1 - "
							+ MAX_NUMBER_OF_PLAYERS);
		else if (numberOfPlayers < 1)
			log().debug(
					"Whooaaa! You need at least 1 player. Must be 1 - "
							+ MAX_NUMBER_OF_PLAYERS);

		players.clear();

		for (int i = 0; i < numberOfPlayers; i++) {
			Player playerToAdd = new Player("Player" + i, 0, -1);
			playerToAdd.setBoard(board);
			players.add(playerToAdd);
		}

		setPlayerOrder();
	}

	public Board getBoard() {
		return board;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public int getNumberOfPlayers() {
		return players.size();
	}

	private void setPlayerOrder() {
		Random roll = new Random();
		int numberOfPlayers = getNumberOfPlayers();
		order = roll.nextInt(numberOfPlayers) + 1;
		for (int i = 0; i < numberOfPlayers; i++) {
			int turn = ((order + i) % (numberOfPlayers)) + 1;
			players.get(i).setTurn(turn);
		}
		resortPlayers();
	}

	private void resortPlayers() {
		ArrayList<Player> secondPlayers = Lists.newArrayList(players);
		for (Player aPlayer : players) {
			secondPlayers.set(aPlayer.getTurn() - 1, aPlayer);
		}
		players = secondPlayers;
	}

	public void update() {
		for (Player aPlayer : players) {
			if(aPlayer.hasTurn())
			aPlayer.takeTurn(spinner.getSpin());
		}

	}

	public boolean hasWinningPlayer() {
		for (Player aPlayer : players) {
			if (aPlayer.isOnLastTile()) {
				foundPlayer = true;
				log().debug("Player Has Landed on Tile 100!! " + foundPlayer);
			}
		}
		return foundPlayer;
	}

}
