package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import playn.core.Image;
import playn.core.ImageLayer;

public class TitleSprite {

	private ImageLayer layer;

	public TitleSprite(String imagePath) {
		Image titleImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(titleImage);
	}

	public ImageLayer getLayer() {
		return layer;
	}

}
