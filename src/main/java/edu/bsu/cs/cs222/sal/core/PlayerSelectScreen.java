package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import pythagoras.f.Point;
import react.SignalView;
import react.UnitSignal;
import react.UnitSlot;
import tripleplay.game.UIAnimScreen;
import tripleplay.ui.Button;
import tripleplay.ui.Root;
import tripleplay.ui.SimpleStyles;
import tripleplay.ui.layout.AbsoluteLayout;

public class PlayerSelectScreen extends UIAnimScreen {

	private Root root;
	public static int numbPlayers = 0;
	//GameScreen game = new GameScreen();
	PlayerSelectSprite background = new PlayerSelectSprite(
			"images/SELECT_PLAYERS_SCREEN1.gif");
	private UnitSignal player1 = new UnitSignal();
	private UnitSignal player2 = new UnitSignal();
	private UnitSignal player3 = new UnitSignal();
	private UnitSignal player4 = new UnitSignal();

	public SignalView<Void> on1Player() {
		return player1;
	}

	public SignalView<Void> on2Player() {
		return player2;
	}

	public SignalView<Void> on3Player() {
		return player3;
	}

	public SignalView<Void> on4Player() {
		return player4;
	}

	@Override
	public void wasAdded() {
		
		initRoot();
		player1Button();
		player2Button();
		player3Button();
		player4Button();
		playerSelectBackground();
	
	}

	private void initRoot() {
		root = iface.createRoot(new AbsoluteLayout(), //
				SimpleStyles.newSheet(), //
				layer)//
				.setSize(graphics().width(), graphics().height());
	}

	private void playerSelectBackground() {
		log().debug("Added Background Image");
		background.getLayer().depth();
		layer.add(background.getLayer());
	}

	private void player1Button() {
		Button player1Button = new Button("1 PLAYER");
		player1Button.setVisible(true);
		player1Button.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				numbPlayers = 1;
				player1.emit();
			}
		});
		root.add(AbsoluteLayout.at(player1Button, new Point(65, 475)));

	}

	private void player2Button() {
		Button player2Button = new Button("2 PLAYER");
		player2Button.setVisible(true);
		player2Button.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				numbPlayers = 2;
				player2.emit();
			}
		});
		root.add(AbsoluteLayout.at(player2Button, new Point(195, 475)));

	}

	private void player3Button() {
		Button player3Button = new Button("3 PLAYER");
		player3Button.setVisible(true);
		player3Button.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				numbPlayers = 3;
				player3.emit();
			}
		});
		root.add(AbsoluteLayout.at(player3Button, new Point(335, 475)));
	}

	private void player4Button() {
		Button player4Button = new Button("4 PLAYER");
		player4Button.setVisible(true);
		player4Button.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				numbPlayers = 4;
				player4.emit();
			}
		});
		root.add(AbsoluteLayout.at(player4Button, new Point(485, 475)));

	}

	@Override
	public void wasShown() {
		super.wasShown();
		log().debug("PlayerSelectScreen shown");
	}

	@Override
	public void wasHidden() {
		super.wasHidden();
		log().debug("PlayerSelectScreen hidden");
	}

	@Override
	public void wasRemoved() {
		super.wasRemoved();
		log().debug("PlayerSelectScreen removed");
	}
}
