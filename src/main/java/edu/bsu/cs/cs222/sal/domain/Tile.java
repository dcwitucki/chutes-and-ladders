package edu.bsu.cs.cs222.sal.domain;

import static playn.core.PlayN.log;

public class Tile {
	private Type type;
	private int index;
	private Tile connectedTile;

	public enum Type {
		Blank, Chute, Ladder
	}

	private Tile(Type type) {
		index = -1;
		this.type = type;
	}

	public static Tile getBlankTile() {
		Tile aTile = new Tile(Type.Blank);
		return aTile;
	}

	public static Tile getBlankTileWithIndex(int index) {
		Tile aTile = new Tile(Type.Blank);
		aTile.setIndex(index);
		return aTile;
	}

	public Type getType() {
		return type;
	}

	public void setTileTypeBlank() {
		connectedTile = null;
		type = Type.Blank;
	}

	public void setTileTypeLadder(Tile connectingTile) {
		if (!isIndexSet()) {
			log().debug("Index of this Tile is not set");

		}
		if (!connectingTile.isIndexSet()) {
			log().debug("Index of connecting tile is not set");

		}
		if (!(index < connectingTile.index)) {
			log().debug(
					"Can not make a ladder type to a connecting tile that has a lower index.");
		}
		type = Type.Ladder;
		connectedTile = connectingTile;

	}

	public void setTileTypeChute(Tile connectingTile) throws Exception {
		if (!isIndexSet()) {
			log().debug("Index of this Tile is not set");

		}
		if (!connectingTile.isIndexSet()) {
			log().debug("Index of connecting tile is not set");

		}
		if (!(index > connectingTile.index)) {
			log().debug(
					"Can not make a chute type to a connecting tile that has a higher index.");
		}
		type = Type.Chute;
		connectedTile = connectingTile;

	}

	public boolean hasConnectedTile() {
		return !type.equals(Type.Blank);
	}

	// should call hasConnedTile() first and it must return true
	public Tile getConnectedTile() {
		if (!type.equals(Type.Chute) && !type.equals(Type.Ladder)) {
			log().debug("Tile Has No Connected Tile");
		}

		return connectedTile;

	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isIndexSet() {
		return !(index == -1);
	}

	public int getIndex() {
		return index;
	}
}
