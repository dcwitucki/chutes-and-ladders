package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import playn.core.Image;
import playn.core.ImageLayer;
import pythagoras.f.Point;
import react.SignalView;
import react.UnitSignal;
import react.UnitSlot;
import tripleplay.game.UIAnimScreen;
import tripleplay.ui.Button;
import tripleplay.ui.Root;
import tripleplay.ui.SimpleStyles;
import tripleplay.ui.layout.AbsoluteLayout;

public class WinnerScreen extends UIAnimScreen {

	private ImageLayer imageLayer;
	private WinnerScreen winnerScreen;
	private Root root;

	TitleSprite player1Winner = new TitleSprite("images/PLAYER1_WINNER.png");
	TitleSprite player2Winner = new TitleSprite("images/PLAYER2_WINNER.png");
	TitleSprite player3Winner = new TitleSprite("images/PLAYER4_WINNER.png");
	TitleSprite player4Winner = new TitleSprite("images/PLAYER3_WINNER.png");
	TitleSprite exitButton = new TitleSprite("images/EXIT_BUTTON.png");	

	private UnitSignal onRestart = new UnitSignal();
	private UnitSignal onMainMenu = new UnitSignal();

	public WinnerScreen(String imagePath) {
		Image titleImage = assets().getImage(imagePath);
		imageLayer = graphics().createImageLayer(titleImage);
	}

	public WinnerScreen() {

	}

	public SignalView<Void> onRestart() {
		return onRestart;
	}

	public SignalView<Void> onMainMenu() {
		return onMainMenu;
	}

	public void winnerBackground() {
		log().debug("Added Background Image");
		winnerScreen = new WinnerScreen("images/WINNER_SCREEN.gif");
		layer.add(winnerScreen.getLayer());
	}

	/*
	 * private void addRestartButton() { Button exitButton = new
	 * Button("Replay"); exitButton.setVisible(true); exitButton.onClick(new
	 * UnitSlot() {
	 * 
	 * @Override public void onEmit() { onRestart.emit(); } });
	 * root.add(AbsoluteLayout.at(exitButton, new Point(150, 300)));
	 * 
	 * }
	 */

//	private void addBackButton() {
//		Button backButton = new Button("Main Menu");
//		backButton.setVisible(true);
//		backButton.onClick(new UnitSlot() {
//			@Override
//			public void onEmit() {
//				log().debug("Exiting WinningScreen and going to TItleScreen");
//				onMainMenu.emit();
//			}
//		});
//		root.add(AbsoluteLayout.at(backButton, new Point(150, 375)));
//
//	}

	private void addExitGame() {
		Button backButton = new Button("Exit Game------------ \n");
		backButton.setVisible(true);
		backButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				log().debug("Exiting Game");
				System.exit(0);
			}
		});
		root.add(AbsoluteLayout.at(backButton, new Point(245, 450)));

	}

	public void addWinnerImage() {
		if (PlayerSelectScreen.numbPlayers == 1) {
			if (GameScreen.count == 0) {
				layer.add(player1Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 1 Winner Image");
			}
		}
		else if (PlayerSelectScreen.numbPlayers == 2) {
			if (GameScreen.count == 0) {
				layer.add(player1Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 1 Winner Image");
			} else if (GameScreen.count == 1) {
				layer.add(player2Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 2 Winner Image");
			}
		}
		else if (PlayerSelectScreen.numbPlayers == 3) {
			if (GameScreen.count == 0) {
				layer.add(player1Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 1 Winner Image");
			} else if (GameScreen.count == 1) {
				layer.add(player2Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 2 Winner Image");
			} else if (GameScreen.count == 2) {
				layer.add(player3Winner.getLayer().setOrigin(-145, -150));
				log().debug("Added Player 3 Winner Image");
			}
		} else if (PlayerSelectScreen.numbPlayers == 4) {
			if (GameScreen.count == 0) {
				layer.add(player1Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 1 Winner Image");
			} else if (GameScreen.count == 1) {
				layer.add(player2Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 2 Winner Image");
			} else if (GameScreen.count == 2) {
				layer.add(player3Winner.getLayer().setOrigin(-145, -150));
				log().debug("Added Player 3 Winner Image");
			} else if (GameScreen.count == 3) {
				layer.add(player4Winner.getLayer().setOrigin(-185, -150));
				log().debug("Added Player 4 Winner Image");
			}
		}
	}
	
	private void addExitButton(){
		layer.add(exitButton.getLayer().setOrigin(-245, -450));
	}

	private void initRoot() {
		root = iface.createRoot(new AbsoluteLayout(), //
				SimpleStyles.newSheet(), //
				layer)//
				.setSize(graphics().width(), graphics().height());
	}

	@Override
	public void wasAdded() {
		winnerBackground();
		initRoot();
		addExitButton();
		addExitGame();
		addWinnerImage();
		// addRestartButton();
		// addBackButton();
	}

	public ImageLayer getLayer() {
		return imageLayer;
	}

	@Override
	public void wasShown() {
		super.wasShown();
		log().debug("WinnerScreen shown");
	}

	@Override
	public void wasHidden() {
		super.wasHidden();
		log().debug("WinnerScreen hidden");
	}

	@Override
	public void wasRemoved() {
		super.wasRemoved();
		log().debug("WinnerScreen removed");
	}

}
