package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.graphics;
import static playn.core.PlayN.log;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import playn.core.Color;
import playn.core.GroupLayer;
import playn.core.ImageLayer;
import pythagoras.f.Point;
import react.SignalView;
import react.UnitSignal;
import react.UnitSlot;
import tripleplay.game.UIAnimScreen;
import tripleplay.ui.Button;
import tripleplay.ui.Label;
import tripleplay.ui.Root;
import tripleplay.ui.SimpleStyles;
import tripleplay.ui.Style;
import tripleplay.ui.layout.AbsoluteLayout;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import edu.bsu.cs.cs222.sal.domain.ChutesAndLaddersGame;
import edu.bsu.cs.cs222.sal.domain.Player;
import edu.bsu.cs.cs222.sal.domain.Spinner;

public class GameScreen extends UIAnimScreen {
	private Root root;
	public int players = 0;
	private ChutesAndLaddersGame game;
	private GameScreen gameName;
	public static ArrayList<PlayerSprite> playerSprites;
	private BoardSprite boardSprite;
	private static final float ANIMATION_TIME = 1000f;
	private static final float SPIN_TIME = 10f;
	public static int count = 0;
	public static int playerCount = 1;
	private int spin = 0;
	private int turnCounter = 0;
	private static String name1, name2, name3, name4;
	private static String player1 = "";
	private static String player2 = "";
	private static String player3 = "";
	private static String player4 = "";
	private Label playerOne;
	private Label playerTwo;
	private Label playerThree;
	private Label playerFour;
	private UnitSignal onGameScreen = new UnitSignal();
	private UnitSignal backToTitle = new UnitSignal();
	GroupLayer turnGroup = graphics().createGroupLayer();
	GroupLayer spinnerGroup = graphics().createGroupLayer();
	SpinnerSprite player1Image = new SpinnerSprite("images/PLAYER1.png");
	SpinnerSprite player2Image = new SpinnerSprite("images/PLAYER2.png");
	SpinnerSprite player3Image = new SpinnerSprite("images/PLAYER3.png");
	SpinnerSprite player4Image = new SpinnerSprite("images/PLAYER4.png");
	SpinnerSprite spinButton = new SpinnerSprite("images/SPIN_BUTTON.png");
	SpinnerSprite exitButton = new SpinnerSprite("images/EXIT_BUTTON.png");
	SpinnerSprite spinnerSprite1 = new SpinnerSprite("images/SPINNER_1.png");
	SpinnerSprite spinnerSprite2 = new SpinnerSprite("images/SPINNER_2.png");
	SpinnerSprite spinnerSprite3 = new SpinnerSprite("images/SPINNER_3.png");
	SpinnerSprite spinnerSprite4 = new SpinnerSprite("images/SPINNER_4.png");
	SpinnerSprite spinnerSprite5 = new SpinnerSprite("images/SPINNER_5.png");
	SpinnerSprite spinnerSprite6 = new SpinnerSprite("images/SPINNER_6.png");
	TitleSprite player1Winner = new TitleSprite("images/PLAYER1_WINNER.png");
	TitleSprite player2Winner = new TitleSprite("images/PLAYER2_WINNER.png");
	TitleSprite player3Winner = new TitleSprite("images/PLAYER4_WINNER.png");
	TitleSprite player4Winner = new TitleSprite("images/PLAYER3_WINNER.png");

	// TitleSprite exitButton = new TitleSprite("images/EXIT_BUTTON.png");

	public SignalView<Void> onGameScreen() {
		return onGameScreen;
	}

	public SignalView<Void> backToTitle() {
		return backToTitle;
	}

	@Override
	public void wasAdded() {
		initRoot();
		game = new ChutesAndLaddersGame();
		try {
			game.setNumberOfPlayers(PlayerSelectScreen.numbPlayers);
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@  User Selected @@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@  " + PlayerSelectScreen.numbPlayers + " Player Mode  @@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		} catch (Exception e) {
			e.printStackTrace();
		}

		initPlayerSprites();
		initSpinner();
		addPlayerImages();
		initNameLabel();
		initBoard();
		addButtons();
		initSpinButton();
		initExitButton();
		setPlayerLocations();
		setPlayersTurnGraphic();
		log().debug("Game Screen Created");
	}

	private void initRoot() {
		root = iface.createRoot(new AbsoluteLayout(), //
				SimpleStyles.newSheet(), //
				layer)//
				.setSize(graphics().width(), graphics().height());
	}

	private void initBoard() {
		boardSprite = new BoardSprite(game.getBoard(),
				"images/FinalGameBoard.jpg");
		layer.add(boardSprite.getLayer());
	}

	public GameScreen() {

	}

	public GameScreen(Root root) {
		player1 = name1;
		playerOne = new Label().addStyles(Style.COLOR.is(Color.argb(255, 255,
				255, 0)));

		player2 = name2;
		playerTwo = new Label().addStyles(Style.COLOR.is(Color.argb(255, 255,
				255, 0)));

		player3 = name3;
		playerThree = new Label().addStyles(Style.COLOR.is(Color.argb(255, 255,
				255, 0)));

		player4 = name4;
		playerFour = new Label().addStyles(Style.COLOR.is(Color.argb(255, 255,
				255, 0)));

		addLabel(root);
		updatePlayerNames();
	}

	private void addLabel(Root root) {
		if (PlayerSelectScreen.numbPlayers == 1) {
			root.add(AbsoluteLayout.at(playerOne, new Point(515, 260)));
		}
		if (PlayerSelectScreen.numbPlayers == 2) {
			root.add(AbsoluteLayout.at(playerOne, new Point(515, 260)));
			root.add(AbsoluteLayout.at(playerTwo, new Point(515, 295)));
		}
		if (PlayerSelectScreen.numbPlayers == 3) {
			root.add(AbsoluteLayout.at(playerOne, new Point(515, 260)));
			root.add(AbsoluteLayout.at(playerTwo, new Point(515, 295)));
			root.add(AbsoluteLayout.at(playerThree, new Point(515, 330)));
		}

		if (PlayerSelectScreen.numbPlayers == 4) {
			root.add(AbsoluteLayout.at(playerOne, new Point(515, 260)));
			root.add(AbsoluteLayout.at(playerTwo, new Point(515, 295)));
			root.add(AbsoluteLayout.at(playerThree, new Point(515, 330)));
			root.add(AbsoluteLayout.at(playerFour, new Point(515, 365)));
		}
	}

	private void initNameLabel() {
		setGameName(new GameScreen(root));
	}

	private void updatePlayerNames() {
		log().debug("Loaded Player1 Name --" + player1 + "--");
		playerOne.text.update(player1);
		log().debug("Loaded Player2 Name --" + player2 + "--");
		playerTwo.text.update(player2);
		log().debug("Loaded Player3 Name --" + player3 + "--");
		playerThree.text.update(player3);
		log().debug("Loaded Player4 Name --" + player4 + "--");
		log().debug("---------------------------------------");
		playerFour.text.update(player4);

	}

	private void addPlayerImages() {
		if (PlayerSelectScreen.numbPlayers == 1) {
			layer.add(player1Image.getLayer().setOrigin(-625, -260));
			log().debug("Loaded Player Display with 1 Player");
		}
		if (PlayerSelectScreen.numbPlayers == 2) {
			layer.add(player1Image.getLayer().setOrigin(-625, -260));
			layer.add(player2Image.getLayer().setOrigin(-625, -295));
			log().debug("Loaded Player Display with 2 Player");
		}
		if (PlayerSelectScreen.numbPlayers == 3) {
			layer.add(player1Image.getLayer().setOrigin(-625, -260));
			layer.add(player2Image.getLayer().setOrigin(-625, -295));
			layer.add(player3Image.getLayer().setOrigin(-625, -330));
			log().debug("Loaded Player Display with 3 Player");
		}
		if (PlayerSelectScreen.numbPlayers == 4) {
			layer.add(player1Image.getLayer().setOrigin(-625, -260));
			layer.add(player2Image.getLayer().setOrigin(-625, -295));
			layer.add(player3Image.getLayer().setOrigin(-625, -330));
			layer.add(player4Image.getLayer().setOrigin(-625, -365));
			log().debug("Loaded Player Display with 4 Player");
		}
	}

	private void initSpinButton() {
		Button spinButton = new Button("Spin this now ------- \n");
		spinButton.setVisible(true);
		spinButton.onClick(new UnitSlot() {
			@Override
			public void onEmit() {
				makeNextMove();
			}
		});
		root.add(AbsoluteLayout.at(spinButton, new Point(100, 525)));
	}

	private void addButtons() {
		layer.add(spinButton.getLayer().setOrigin(-99, -525));
		layer.add(exitButton.getLayer().setOrigin(-299, -525));
	}

	private void initExitButton() {
		Button spinButton = new Button("Exit this now -------- \n");
		spinButton.setVisible(true);
		spinButton.onClick(new UnitSlot() {

			@Override
			public void onEmit() {
				int response = JOptionPane.showConfirmDialog(null,
						"Are you sure you want to quit?");
				if (response == 0) {
					System.exit(0);
				} else if (response == 1) {
					return;
				} else {
					return;
				}
			}

		});

		root.add(AbsoluteLayout.at(spinButton, new Point(300, 525)));
	}

	private void makeNextMove() {
		log().debug("---------------------------");
		Spinner spinner = new Spinner();
		spin = spinner.getSpin();
		spinSpinner();
		setPlayersTurnGraphic();
		if (count == 0) {
			playerSprites.get(0).getPlayer().takeTurn(spin);
		
			spin = spinner.getSpin();
			movePlayer1();
		}
		if (count == 1) {
			playerSprites.get(1).getPlayer().takeTurn(spin);
		
			spin = spinner.getSpin();
			movePlayer2();

		}
		if (count == 2) {
			playerSprites.get(2).getPlayer().takeTurn(spin);
			
			spin = spinner.getSpin();
			movePlayer3();

		}
		if (count == 3) {
			playerSprites.get(3).getPlayer().takeTurn(spin);
			
			movePlayer4();
		}
		if (game.hasWinningPlayer() == true) {
			finalMove();
		}
		count++;

		if (PlayerSelectScreen.numbPlayers - 1 < count) {
			count = 0;

		}
	}

	private void initPlayerSprites() {
		playerSprites = Lists.newArrayList();
		for (Player aPlayer : game.getPlayers()) {
			if (playerCount == 1) {
				PlayerSprite playerSprite1 = new PlayerSprite(aPlayer,
						"images/PLAYER1.png");
				playerSprites.add(playerSprite1);
				name1 = JOptionPane
						.showInputDialog("Please Enter Player 1's Name:");
				aPlayer.setName(name1);
				layer.add(playerSprite1.getLayer());

			} else if (playerCount == 2) {
				PlayerSprite playerSprite2 = new PlayerSprite(aPlayer,
						"images/PLAYER2.png");
				playerSprites.add(playerSprite2);
				name2 = JOptionPane
						.showInputDialog("Please Enter Player 2's Name:");
				aPlayer.setName(name2);
				layer.add(playerSprite2.getLayer());

			} else if (playerCount == 3) {
				PlayerSprite playerSprite3 = new PlayerSprite(aPlayer,
						"images/PLAYER3.png");
				playerSprites.add(playerSprite3);
				name3 = JOptionPane
						.showInputDialog("Please Enter Player 3's Name:");
				aPlayer.setName(name3);
				layer.add(playerSprite3.getLayer());

			} else if (playerCount == 4) {
				PlayerSprite playerSprite4 = new PlayerSprite(aPlayer,
						"images/PLAYER4.png");
				playerSprites.add(playerSprite4);
				name4 = JOptionPane
						.showInputDialog("Please Enter Player 4's Name:");
				aPlayer.setName(name4);
				layer.add(playerSprite4.getLayer());

			}
			playerCount++;
		}
	}

	private void setPlayersTurnGraphic() {
		turnGroup.setDepth(2);
		turnGroup.setScale(.3f, .3f);
		turnGroup.setTranslation(530f, 65f);
		turnGroup.setVisible(true);

		if (PlayerSelectScreen.numbPlayers == 1) {
			turnGroup.add(player1Winner.getLayer());
			log().debug("------------------------------------");
			log().debug("-------It is " + player1 +" turn-------");
			log().debug("------------------------------------");
		} else if (PlayerSelectScreen.numbPlayers == 2) {
			if (turnCounter == 0) {
				turnGroup.clear();
				turnGroup.add(player1Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player1 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;

			} else if (turnCounter == 1) {
				turnGroup.clear();
				turnGroup.add(player2Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player2 +" turn-------");
				log().debug("------------------------------------");
				turnCounter = 0;
			}

		}

		else if (PlayerSelectScreen.numbPlayers == 3) {
			if (turnCounter == 0) {
				turnGroup.clear();
				turnGroup.add(player1Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player1 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;
			} else if (turnCounter == 1) {
				turnGroup.clear();
				turnGroup.add(player2Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player2 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;
			} else if (turnCounter == 2) {
				turnGroup.clear();
				turnGroup.add(player3Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player3 +" turn-------");
				log().debug("------------------------------------");
				turnCounter = 0;
			}
		}

		else if (PlayerSelectScreen.numbPlayers == 4) {
			if (turnCounter == 0) {
				turnGroup.clear();
				turnGroup.add(player1Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player1 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;
			} else if (turnCounter == 1) {
				turnGroup.clear();
				turnGroup.add(player2Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player2 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;
			} else if (turnCounter == 2) {
				turnGroup.clear();
				turnGroup.add(player3Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player3 +" turn-------");
				log().debug("------------------------------------");
				turnCounter++;
			} else if (turnCounter == 3) {
				turnGroup.clear();
				turnGroup.add(player4Winner.getLayer());
				log().debug("------------------------------------");
				log().debug("-------It is " + player4 +" turn-------");
				log().debug("------------------------------------");
				turnCounter = 0;
			}
		}
		layer.add(turnGroup);
	}

	private void initSpinner() {
		spinnerGroup.add(spinnerSprite1.getLayer());
		spinnerGroup.setDepth(1);
		spinnerGroup.setTranslation(575, 520);
		spinnerGroup.setOrigin(spinnerSprite1.getLayer().width() / 2,
		spinnerSprite1.getLayer().height() / 2);
		layer.add(spinnerGroup);

	}

	private void animateSpinner(ImageLayer layer) {
		Preconditions.checkNotNull(layer);
		spinnerGroup.setRotation(14f);
		anim.tweenRotation(spinnerGroup);
		spinnerGroup.clear();
		if (spin == 0) {
			spinnerGroup.add(spinnerSprite1.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@@ Spinner Initiated @@@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite1 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 1) {
			spinnerGroup.add(spinnerSprite1.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 1 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite1 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 2) {
			spinnerGroup.add(spinnerSprite2.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 2 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite2 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 3) {
			spinnerGroup.add(spinnerSprite3.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 3 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite3 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 4) {
			spinnerGroup.add(spinnerSprite4.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 4 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite4 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 5) {
			spinnerGroup.add(spinnerSprite5.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 5 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite5 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		if (spin == 6) {
			spinnerGroup.add(spinnerSprite6.getLayer());
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@@@@@@ Spinner Landed on 6 @@@@@@@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			log().debug("@@@@ Added spinnerSprite6 to Layer @@@@");
			log().debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}

	}

	private void spinSpinner() {
		animateSpinner(spinnerSprite1.getLayer());

	}

	private void setPlayerLocations() {

		log().debug("--------------------------------");
		log().debug("----  Set Player Locations  ----");
		log().debug("--------------------------------");
		for (PlayerSprite aPlayerSprite : playerSprites) {
			Player aPlayer = aPlayerSprite.getPlayer();

			for (int position : aPlayer.getListOfMoveIteration()) {
				Point playerPoint = boardSprite.getPointForPlayer(
						aPlayer.getTurn(), position);
				spinSpinner();

				animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
						ANIMATION_TIME);
			}
		}
	}

	private void movePlayer1() {
		PlayerSprite aPlayerSprite = playerSprites.get(count);
		Player aPlayer = aPlayerSprite.getPlayer();
		for (int position : aPlayer.getListOfMoveIteration()) {
			Point playerPoint = boardSprite.getPointForPlayer(
					aPlayer.getTurn(), position);
			animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
					ANIMATION_TIME);
		}
	}

	private void movePlayer2() {
		PlayerSprite aPlayerSprite = playerSprites.get(count);
		Player aPlayer = aPlayerSprite.getPlayer();
		for (int position : aPlayer.getListOfMoveIteration()) {
			Point playerPoint = boardSprite.getPointForPlayer(
					aPlayer.getTurn(), position);
			animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
					ANIMATION_TIME);
		}
	}

	private void movePlayer3() {
		PlayerSprite aPlayerSprite = playerSprites.get(count);
		Player aPlayer = aPlayerSprite.getPlayer();
		for (int position : aPlayer.getListOfMoveIteration()) {
			Point playerPoint = boardSprite.getPointForPlayer(
					aPlayer.getTurn(), position);
			animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
					ANIMATION_TIME);
		}
	}

	private void movePlayer4() {
		PlayerSprite aPlayerSprite = playerSprites.get(count);
		Player aPlayer = aPlayerSprite.getPlayer();
		for (int position : aPlayer.getListOfMoveIteration()) {
			Point playerPoint = boardSprite.getPointForPlayer(
					aPlayer.getTurn(), position);
			animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
					ANIMATION_TIME);
		}
	}

	private void finalMove() {
		int counter = 0;
		while (counter < 1) {
			PlayerSprite aPlayerSprite = playerSprites.get(count);
			Player aPlayer = aPlayerSprite.getPlayer();
			for (int position : aPlayer.getListOfMoveIteration()) {
				Point playerPoint = boardSprite.getPointForPlayer(
						aPlayer.getTurn(), position);
				animateSpriteTranslation(aPlayerSprite.getLayer(), playerPoint,
						ANIMATION_TIME);
				counter++;
			}
		}
		onGameScreen.emit();
	}

	private void animateSpriteTranslation(ImageLayer layer, Point location,
			float time) {
		Preconditions.checkNotNull(layer);
		anim.delay(ANIMATION_TIME).then().tweenTranslation(layer)
				.to(location.x, location.y).in(time);
		anim.delay(ANIMATION_TIME).then().tweenRotation(layer).from(SPIN_TIME)
				.then().shake(layer);
		anim.addBarrier(ANIMATION_TIME);
	}

	@Override
	public void wasShown() {
		super.wasShown();
		log().debug("GameScreen shown");
	}

	@Override
	public void wasHidden() {
		super.wasHidden();
		log().debug("GameScreen hidden");
	}

	@Override
	public void wasRemoved() {
		super.wasRemoved();
		log().debug("GameScreen removed");
	}

	public GameScreen getGameName() {
		return gameName;
	}

	public void setGameName(GameScreen gameName) {
		this.gameName = gameName;
	}
}
