package edu.bsu.cs.cs222.sal.core;

import tripleplay.game.ScreenStack;
import tripleplay.game.UIAnimScreen;
import tripleplay.game.UIScreen;
import tripleplay.ui.Root;

public class MainScreen extends UIScreen {
	protected final UIAnimScreen[] screens;
	protected final ScreenStack stack;
	protected Root root;

	public MainScreen(ScreenStack stack) {
		this.stack = stack;
		this.screens = new UIAnimScreen[] { new GameScreen(), };
	}

	@Override
	public void wasShown() {
		super.wasShown();
		stack.push(screens[0], ScreenStack.NOOP);
	}

}
