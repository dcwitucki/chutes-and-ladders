package edu.bsu.cs.cs222.sal.core.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.bsu.cs.cs222.sal.domain.ChutesAndLaddersGame;

public class GameTests {

	@Test
	public void ConstructGameTest() {
		ChutesAndLaddersGame myGame = new ChutesAndLaddersGame();
		assertNotNull(myGame);
	}

	@Test
	public void setNumberOfPlayersTest() throws Exception {
		ChutesAndLaddersGame myGame = new ChutesAndLaddersGame();
		myGame.setNumberOfPlayers(3);
		int actual = myGame.getNumberOfPlayers();
		int expected = 3;
		assertEquals(expected, actual);
	}

	@Test
	public void setPlayerOrderTest() {
		ChutesAndLaddersGame myGame = new ChutesAndLaddersGame();
		try {
			myGame.setNumberOfPlayers(4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
