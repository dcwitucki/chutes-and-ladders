package edu.bsu.cs.cs222.sal.core;

import playn.core.Image;
import playn.core.ImageLayer;
import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;

public class WinnerSprite {

	private ImageLayer layer;
	
	public WinnerSprite(String imagePath){
		Image winnerImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(winnerImage);
	}
	
	public ImageLayer getLayer(){
		return layer;
	}
}
