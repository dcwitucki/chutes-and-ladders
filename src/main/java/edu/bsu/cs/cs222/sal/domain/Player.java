package edu.bsu.cs.cs222.sal.domain;

import static playn.core.PlayN.log;

import java.util.ArrayList;

import com.google.common.collect.Lists;

public class Player {

	private String name;
	private int currentPosition;
	private ArrayList<Integer> allMovesLastMove;
	private int turn;
	private Board board;
	

	public Player(String name, int position, int turn) {
		this.name = name;
		currentPosition  = position;
		allMovesLastMove = Lists.newArrayList();
		allMovesLastMove.add(position);
		this.turn = turn;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public void setName(String nameInput) {
		name = nameInput;
	}

	public String getName() {
		return name;
	}

	public void takeTurn(int NumberToMove) {
		allMovesLastMove.clear();
		log().debug("Turn Taken by " + this.name);
		if (currentPosition + NumberToMove > board.getTiles().size()) {
			return;
		}
		currentPosition += NumberToMove;
		allMovesLastMove.add(currentPosition);
		moveToConnectedTile();

	}

	public void moveToConnectedTile() {
		Tile aTile = board.getTiles().get(currentPosition - 1);

		if (aTile.hasConnectedTile()) {
			try {
				Tile connectingTile = board.getTiles().get(currentPosition - 1)
						.getConnectedTile();
				currentPosition = connectingTile.getIndex();
				allMovesLastMove.add(currentPosition);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
	}
	
	public ArrayList<Integer> getListOfMoveIteration(){
		return allMovesLastMove;
	}

	public boolean isOnLastTile() {
		return currentPosition == board.getTiles().size();
			
		
	}
	

	public int getPosition() {
		return currentPosition;
	}

	public void setTurn(int awardedTurn) {
		turn = awardedTurn;
	}

	public int getTurn() {
		return turn;
	}

	public boolean hasTurn() {
		return true;
	}
}
