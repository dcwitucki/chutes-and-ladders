package edu.bsu.cs.cs222.sal.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import playn.core.Image;
import playn.core.ImageLayer;
import edu.bsu.cs.cs222.sal.domain.Player;

public class PlayerSprite {
	private Player player;
	private ImageLayer layer;

	public PlayerSprite(Player aPlayer, String imagePath) {
		this.player = aPlayer;
		Image playerImage = assets().getImage(imagePath);
		layer = graphics().createImageLayer(playerImage);
		layer.setDepth(1);
	}

	public ImageLayer getLayer() {
		return layer;
	}

	public Player getPlayer() {
		return player;
	}
}
